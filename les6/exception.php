<?php
    function checkNum ($number){
        if (!filter_var($number, FILTER_VALIDATE_INT)) {
            throw new \Exception('De waarde is geen geheel getal.');
        }
    }
    
    try {
        checkNum("zes");
    } catch (\Exception $e) {
        echo 'Message: ' . $e->getMessage();
    }