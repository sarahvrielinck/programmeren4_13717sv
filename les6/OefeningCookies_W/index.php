<?php
    $cookieName = "boughtProducts";
    
    if (isset($_POST['products']))
    {
        $cookieValue = unserialize($_COOKIE[$cookieName]);
        $cookieValue[] = $_POST['products'];
        setcookie($cookieName, serialize($cookieValue), time()+60*60*24*7, "/"); // cookie geldig voor 7 dagen over gans het domein
        $_COOKIE[$cookieName] = serialize($cookieValue);
    }
    
    $cookieValue = unserialize($_COOKIE[$cookieName]);
?>

<form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
    <span id="productsErr"></span>
    <select name="products" id="products">
            <option value="0">Chocolate Chip Cookies</option>
            <option value="1">Chocolate Cookies</option>
            <option value="2">Oatmeal Chocolate Chip Cookies</option>
            <option value="3">Peanut Butter Cookies</option>
            <option value="4">Snickerdoodles</option>
            <option value="5">Sugar Cookies</option>
            <option value="6">Ginger Cookies</option>
            <option value="7">Oatmeal Raisin Cookies</option>
            <option value="8">Sand Cookies</option>
            <option value="9">Raspberry Cookies</option>
        </select>
    <button type="submit">Buy cookies</button>
</form>

<div>
    <table>
        <tr>
            <th>Bought products</th>
        </tr>
            <?php
                foreach($cookieValue as $item) {
                    switch ($item) {
                        case 0:
                            $item = "Chocolate Chip Cookies";
                            break;
                        case 1:
                            $item = "Chocolate Cookies";
                            break;
                        case 2:
                            $item = "Oatmeal Chocolate Chip Cookies";
                            break;
                        case 3:
                            $item = "Peanut Butter Cookies";
                            break;
                        case 4:
                            $item = "Snickerdoodles";
                            break;
                        case 5:
                            $item = "Sugar Cookies";
                            break;
                        case 6:
                            $item = "Ginger Cookies";
                            break;
                        case 7:
                            $item = "Oatmeal Raisin Cookies";
                            break;
                        case 8:
                            $item = "Sand Cookies";
                            break;
                        case 9:
                            $item = "Raspberry Cookies";
                            break;
                        default:
                            $boughtItemsErr = "Something went wrong!";
                            break;
                        }
                    echo "<tr><td>$item<td></tr>";
                }
            ?>
    </table>
</div>