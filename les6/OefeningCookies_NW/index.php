<?php
$cookie_boughtItems = "boughtItems";
$cookie_boughtItems_value = "";
$boughtItemsErr = "";

if(isset($_POST["products"])) {
    $boughtItems = unserialize($_COOKIE[$cookie_boughtItems]);
    switch ($_POST["products"]) {
        case 0:
            $boughtItems[] = "Chocolate Chip Cookies";
            break;
        case 1:
            $boughtItems[] = "Chocolate Cookies";
            break;
        case 2:
            $boughtItems[] = "Oatmeal Chocolate Chip Cookies";
            break;
        case 3:
            $boughtItems[] = "Peanut Butter Cookies";
            break;
        case 4:
            $boughtItems[] = "Snickerdoodles";
            break;
        case 5:
            $boughtItems[] = "Sugar Cookies";
            break;
        case 6:
            $boughtItems[] = "Ginger Cookies";
            break;
        case 7:
            $boughtItems[] = "Oatmeal Raisin Cookies";
            break;
        case 8:
            $boughtItems[] = "Sand Cookies";
            break;
        case 9:
            $boughtItems[] = "Raspberry Cookies";
            break;
        default:
            $boughtItemsErr = "Something went wrong!";
            break;
    }
    $cookie_boughtItems_value = serialize($boughtItems);
    setcookie($cookie_boughtItems, $cookie_boughtItems_value, (86400 * 30) * 7);
    $_COOKIE[$cookie_boughtItems] = $cookie_boughtItems_value;
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Ex. Cookies - mhmm cookies</title>
</head>
<body>
    <h1>Cookie monster shop</h1>
    <div id="cart">
        <?php echo $_COOKIE[$cookie_boughtItems]; ?>
    </div>
    <form action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>" method="post">
        <span id="productsErr"><?php echo $boughtItemsErr; ?></span>
        <select name="products" id="products">
            <option value="0">Chocolate Chip Cookies</option>
            <option value="1">Chocolate Cookies</option>
            <option value="2">Oatmeal Chocolate Chip Cookies</option>
            <option value="3">Peanut Butter Cookies</option>
            <option value="4">Snickerdoodles</option>
            <option value="5">Sugar Cookies</option>
            <option value="6">Ginger Cookies</option>
            <option value="7">Oatmeal Raisin Cookies</option>
            <option value="8">Sand Cookies</option>
            <option value="9">Raspberry Cookies</option>
        </select>
        <button type="submit">Buy cookies</button>
    </form>
    <div id="bought">
        <table>
            <tr>
                <th>Bought items</th>
            </tr>
            <tr>
            <?php
                if(isset($_COOKIE[$cookie_boughtItems])){
                    $result = unserialize($_COOKIE[$cookie_boughtItems]);
                    foreach ($result as $value) {
                        echo "<td>$value</td>";
                    }
                }
            ?>
            </tr>
        </table>
    </div>
</body>
</html>