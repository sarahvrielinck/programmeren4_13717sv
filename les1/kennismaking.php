<!doctype html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>
    <h1>Kennismaking</h1>
    <h2>PHP</h2>
    
    <!-- Post methodiek, wijzigd iets op de server.
         Get methodiek, haalt iets van de server.
    -->
    <form action="verwerken.php" method="post">
        <div>
            <label for="firstName">Voornaam</label>
            <input type="text" name="firstName" id="firstName"/>
        </div>
        <div>
            <label for="lastName">Familienaam</label>
            <input type="text" name="lastName" id="lastName"/>
        </div>
        <button type="submit">Verzenden</button>
    </form>
</body>
</html>