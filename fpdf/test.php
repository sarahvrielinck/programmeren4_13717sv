<?php
require('vendor/setasign/fpdf/fpdf.php');

class PDF extends FPDF
{
// Load data
function LoadData($file)
{
    $data = explode("\n", file_get_contents($file));
    return $data;
}

// Colored table
function FancyTable($header, $data)
{
    // Colors, line width and bold font
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B');
    // Header
    $w = array(80, 30, 160, 45);
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
    $this->Ln();
    // Color and font restoration
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    // Data
    $fill = false;
    foreach($data as $row)
    {
        $postalCode = explode('|', $row);
        $this->Cell($w[0],6,preg_replace("/\([^)]+\)/","",$postalCode[2]),'L',0,'L',$fill);
        $this->Cell($w[1],6,$postalCode[0],'LR',0,'L',$fill);
        $this->Cell($w[2],6,mb_convert_encoding($postalCode[1], 'ISO-8859-1', "UTF-8"),'LR',0,'L',$fill);
        $this->Ln();
        $fill = !$fill;
        // mb_convert_encoding($postalCode[1], 'ISO-8859-1.', "UTF-8")
        // $this->Cell($w[2],6,utf8_decode($postalCode[1]),'LR',0,'L',$fill);
    }
    // Closing line
    $this->Cell(array_sum($w),0,'','T');
}
}

$pdf = new PDF();
// Column headings
$header = array('Provincie', 'Postcode', 'Stad');
// Data loading
$data = $pdf->LoadData('data/Postcodes.csv');
$pdf->SetFont('Arial','',14);
$pdf->AddPage('L');
$pdf->FancyTable($header,$data);
$pdf->Output();
?>