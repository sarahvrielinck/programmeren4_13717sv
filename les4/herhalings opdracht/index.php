<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Concert Bob Dylan - Inschrijven</title>
</head>
<body>
    <form action="behandelreservatie.php" method="post">
        <fieldset>
            <legend>Persoonlijke gegevens</legend>
            <div>
                <label for="voornaam">Voornaam</label>
                <input type="text" name="firstname" id="voornaam" placeholder="voornaam: bv. John" pattern="^[a-zA-Z]+$" required/>
            </div>
            <div>
                <label for="familienaam">Familienaam</label>
                <input type="text" name="name" id="familienaam" placeholder="familienaam: bv. Do" pattern="^[a-zA-Z]+$" required/>
            </div>
            <div>
                <label for="birthday">Geboortedatum</label>
                <input type="date" name="birthday"  id="birthday" required/>
            </div>
            <div>
                <label for="bankaccount">Rekeningnummer</label>
                <input type="text" name="bankaccount" id="bankaccount" pattern="[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}" required/>
            </div>
            <div>
                <label for="email">Emailadres</label>
                <input type="email" name="email" id="email" placeholder="email: bv. john.do@somewhere.net" required/>
            </div>
        </fieldset>
        
        <fieldset>
            <legend>Ticket gegevens</legend>
            <div>
                <label for="aantalpersonen">Aantal personen</label>
                <select id="aantalpersonen" name="numpersons">
                    <option value="0">1</option>
                    <option value="1">2</option>
                    <option value="2">3</option>
                    <option value="3">4</option>
                    <option value="4">5</option>
                    <option value="5">6</option>
                    <option value="6">7</option>
                    <option value="7">8</option>
                    <option value="8">9</option>
                    <option value="9">10</option>
                </select>
            </div>
            <div>
                <label for="plaats">
                    Plaats
                </label>
                <select id="plaats" name="place">
                    <option value="0">stage</option>
                    <option value="1">tribune</option>
                    <option value="2">zaal</option>
                    <option value="3">balkon</option>
                </select>
            </div>
        </fieldset>
        <button type="submit">Inschrijven!</button>
    </form>
</body>
</html>