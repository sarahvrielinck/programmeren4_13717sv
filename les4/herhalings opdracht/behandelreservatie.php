<?php
    /*
        expected posts:
            firstname
            name
            birthday
            bankaccount 
            email
            numpersons 0-9
            place 0-3
            
    */
    
    $firstName = $name = $birthday = $bankaccount = $email = $numpersons = $place = "";
    
    if (isset($_POST['firstname'])){
        $firstName = $_POST['firstname'];
    }
    
    if (isset($_POST['name'])) {
        $name = $_POST['name'];
    }
    
    if (isset($_POST['birthday'])) {
        $birthday = $_POST['birthday'];
    }
    
    if (isset($_POST['bankaccount'])) {
        $bankaccount = $_POST['bankaccount'];
    }
    
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
    
    if (isset($_POST['numpersons'])) {
        $numpersons = $_POST['numpersons'] + 1;
    }
    
    if (isset($_POST['place'])) {
        $place = $_POST['place'];
    }
?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Bevestiging</title>
    
    <style>
        table {
            margin: 0 auto;
            margin-top: 3em;
        }
        h1 {
            text-align: center;
        }
        th {
            padding: 1em;
            background-color: black;
            color: white;
            font-weight: bold;
            text-align: center;
        }
        td {
            padding: 0.75em;
            text-align: left;
        }
        
        tr:nth-child(2n){
            background-color: #eaebed;
        }
    </style>
</head>
<body>
    <h1>Bevestiging van inschrijven</h1>
    <table>
        <tr>
            <th>Gegevens</th>
            <th>Waarde</th>
        </tr>
        <tr>
            <td>Voornaam</td>
            <td><?php echo $firstName ?></td>
        </tr>
        <tr>
            <td>Familienaam</td>
            <td><?php echo $name ?></td>
        </tr>
        <tr>
            <td>Geboortedatum</td>
            <td><?php echo $birthday ?></td>
        </tr>
        <tr>
            <td>Rekeningnummer</td>
            <td><?php echo $bankaccount ?></td>
        </tr>
        <tr>
            <td>Emailadres</td>
            <td><?php echo $email ?></td>
        </tr>
        <tr>
            <td>Aantal personen</td>
            <td><?php echo $numpersons ?></td>
        </tr>
        <tr>
            <td>Plaats</td>
            <td><?php echo $place ?></td>
        </tr>
    </table>
</body>
</html>