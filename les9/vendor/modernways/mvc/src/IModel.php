<?php
/**
 * Created by PhpStorm.
 * User: Jef Inghelbrecht
 * Date: 22/01/2016
 * Time: 15:31
 */
namespace ModernWays\Mvc;

interface IModel
{
    function getModelState();

    function setModelState(\ModernWays\Dialog\Model\INoticeBoard $modelState);

    function getList();

    function setList($list);

    function isValid();

    // function validRequired($value, $name, $displayName = null);

    // function validDateTime($value, $name, $displayName = null);
}
