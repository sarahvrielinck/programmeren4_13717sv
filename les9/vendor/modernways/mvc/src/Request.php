<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Based on http://lornajane.net/posts/2012/building-a-restful-php-server-understanding-the-request
 * Date: 2/08/2016
 * Time: 11:26
 */
namespace ModernWays\Mvc;

class Request
{
    public $url_elements;
    public $verb;
    public $parameters;

    public function __construct($path = '/Home/Index')
    {
        $this->verb = $_SERVER['REQUEST_METHOD'];
        if (isset($_SERVER['PATH_INFO'])) {
            $this->url_elements = explode('/', $_SERVER['PATH_INFO']);
        } else {
            $this->url_elements = explode('/', $path);
        }
        $this->parseIncomingParams();
        // initialise json as default format
        $this->format = 'json';
        if(isset($this->parameters['format'])) {
            $this->format = $this->parameters['format'];
        }
        return true;
    }

    public function parseIncomingParams() {
        $parameters = array();

        // first of all, pull the GET vars
        if (isset($_SERVER['QUERY_STRING'])) {
            parse_str($_SERVER['QUERY_STRING'], $parameters);
        }

        // now how about PUT/POST bodies? These override what we got from GET
        $body = file_get_contents("php://input");
        $content_type = false;
        if(isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE'];
        }
        switch($content_type) {
            case "application/json":
                $body_params = json_decode($body);
                if($body_params) {
                    foreach($body_params as $param_name => $param_value) {
                        $parameters[$param_name] = $param_value;
                    }
                }
                $this->format = "json";
                break;
            case "application/x-www-form-urlencoded":
                parse_str($body, $postvars);
                foreach($postvars as $field => $value) {
                    $parameters[$field] = $value;

                }
                $this->format = "html";
                break;
            default:
                // we could parse other supported formats here
                break;
        }
        $this->parameters = $parameters;
    }

    public function uc() {
        $uc = '/Home/Index';
        if (isset($this->url_elements[1]) && isset($this->url_elements[2])) {
            // class name starts with uppercase
            $entity = ucfirst($this->url_elements[1]);
            // function name starts with lowercase
            $action = lcfirst($this->url_elements[2]);
            $uc = "{$entity}-{$action}";
            if (isset($this->url_elements[3])) {
                $uc .= "_{$this->url_elements[3]}";
            }
        }
        return $uc;
    }
}