<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5afa17d21b5468897e2937e08b01a60d
{
    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'ModernWays\\Mvc\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'ModernWays\\Mvc\\' => 
        array (
            0 => __DIR__ . '/..' . '/modernways/mvc/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5afa17d21b5468897e2937e08b01a60d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5afa17d21b5468897e2937e08b01a60d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
