<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Online webshop - Toy Cars</title>
</head>

<body>
    <!--main>(header>h1)+(article>nav>(a*2)+section)>footer-->
    <main>
        <header>
            <h1>Toy Cars</h1>
        </header>
        <article>
            <nav>
                <a href="https://p4sarahvrielinck-neonglowstix.c9users.io/test-3p-mvc.php/Home/Inserting">Inserten</a>
                <a href=""></a>
                <section>
                        <?php
                            if (count($model->getList()) > 0) { ?>
                                <table>
                                    <?php
                                        foreach($model->getList() as $item) { ?>
                                            <tr>
                                                <td><?php echo $item['Name'];?></td>
                                                <td><?php echo $item['PurchaseDate'];?></td>
                                                <td><?php echo $item['Price']?></td>
                                                <td><a href="/test-3p-mvc.php/Home/Updating/<?php echo $item['Id'];?>">Update</a></td>
                                                <td><a href="/test-3p-mvc.php/Home/Delete?Id=<?php echo $item['Id'];?>">Delete</a></td>
                                            </tr>
                                    <?php  
                                        }
                                    ?>
                                    
                                </table>
                            <?php    
                            } else { ?>
                                <p>No articles found! q-q</p>
                            <?php
                            }
                            ?>
                </section>
            </nav>
        </article>
        <footer></footer>
    </main>
    <?php $appStateView(); ?>
</body>

</html>
