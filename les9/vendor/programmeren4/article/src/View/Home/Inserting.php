<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Online webshop - Toy Cars</title>
</head>

<body>
    <!--main>(header>h1)+(article>nav>(a*2)+section)>footer-->
    <main>
        <header>
            <h1>Toy Cars</h1>
        </header>
        <article>
            <nav>
                <!-- /Home/Editing werkt ook -->
                <a href="https://p4sarahvrielinck-neonglowstix.c9users.io/test-3p-mvc.php/Home/Editing">Editeren</a>
                <a href=""></a>
                <section>
                        <form method="post" action="/test-3p-mvc.php/Home/Insert">
                            <div><input type="text" name="name" id="name" placeholder="name"/></div>
                            <div><input type="date" name="purchaseDate" id="purchaseDate" placeholder="purchaseDate"/></div>
                            <div><input type="number" step="any" name="price" id="price" placeholder="price"/></div>
                            <div><button type="submit">insert</button></div>
                        </form>
                </section>
            </nav>
        </article>
        <footer></footer>
    </main>
    <?php $appStateView(); ?>
</body>

</html>