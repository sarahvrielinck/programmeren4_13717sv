<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Online webshop - Toy Cars</title>
</head>

<body>
    <!--main>(header>h1)+(article>nav>(a*2)+section)>footer-->
    <main>
        <header>
            <h1>Toy Cars</h1>
        </header>
        <article>
            <nav>
                <!-- /Home/Editing werkt ook -->
                <a href="https://p4sarahvrielinck-neonglowstix.c9users.io/test-3p-mvc.php/Home/Editing">Editeren</a>
                <a href=""></a>
                <section>
                        <form method="post" action="/test-3p-mvc.php/Home/Update">
                            <div><input name="ArticleId" type="text" value="<?php echo $model->getId(); ?>"/></div>
                            <div><input type="text" name="name" id="name" placeholder="name" value="<?php echo $model->getName(); ?>"/></div>
                            <div><input type="date" name="purchaseDate" id="purchaseDate" placeholder="purchaseDate" value="<?php echo $model->getPurchaseDate(); ?>"/></div>
                            <div><input type="number" step="any" name="price" id="price" placeholder="price" value="<?php echo $model->getPrice(); ?>"/></div>
                            <div><button type="submit">insert</button></div>
                        </form>
                </section>
            </nav>
        </article>
        <footer></footer>
    </main>
    <?php $appStateView(); ?>
</body>

</html>