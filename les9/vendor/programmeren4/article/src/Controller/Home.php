<?php
    namespace Programmeren4\Article\Controller;
    
    class Home extends \ModernWays\Mvc\Controller
    {
        private $pdo2;
        private $model;
        
        public function  __construct(\ModernWays\Mvc\Route $route = null, \ModernWays\Dialog\Model\INoticeBoard $noticeBoard = null)
        {
            parent::__construct($route, $noticeBoard);
            $this->noticeBoard->startTimeInKey('PDO Connection');
            try {
                $this->pdo2 = new \PDO('mysql:host=localhost;dbname=sarahvrielinck;charset=utf8', 'root', 'nopnop33');
                $this->noticeBoard->setText("Succes! PDO connected succesfully");
                $this->noticeBoard->setCaption('PDO connection for article');
            } catch (\Exception $e) {
                $this->noticeBoard->setText("{$e->getMessage()} op lijn {$e->getLine()} in bestand {$e->getFile()}");
                $this->noticeBoard->setCaption('PDO connection for article');
                $this->noticeBoard->setCode($e->getCode());
            }
            $this->noticeBoard->log();
            
            $modelState = new \ModernWays\Dialog\Model\NoticeBoard();
            $this->model = new \Programmeren4\Article\Model\Article($noticeBoard);
        }
        
        public function Editing()
        {
            if ($this->pdo2)
            {
                // het modul vullen
                $command = $this->pdo2->query("call ArticleSelectAll");
                $this->model->setList($command->fetchAll(\PDO::FETCH_ASSOC));
            }
            return $this->view("Home", "Editing", $this->model);
        }
        
        public function Inserting()
        {
            return $this->view("Home", "Inserting");
        }
        
        public function Insert()
        {
            if($this->pdo2) {
                if (isset($_POST['name']) && 
                    isset($_POST['purchaseDate']) && 
                    isset($_POST['price'])) {
                    if ($_POST['name'] == "") {
                        echo "<div>Name can't be empty</div>";
                    } else if ($_POST['purchaseDate'] == "") {
                        echo "<div>Please provide a date</div>";
                    } else if ($_POST['price'] == "") {
                        echo "<div>Please provie a price</div>";
                    } else {
                        echo "<div>Name: ". $_POST['name'] ."</div>";
                        echo "<div>PurchaseDate: ". $_POST['purchaseDate'] . "</div>";
                        echo "<div>Price: " . $_POST['price'] . "</div>";
                        
                        $this->model->setName($_POST['name']);
                        $this->model->setPurchaseDate($_POST['purchaseDate']);
                        $this->model->setPrice($_POST['price']);
                        
                         $statement = $this->pdo2->prepare("call ArticleInsert(:pName, 
                            :pPurchaseDate, :pPrice, @pId)");
                        // bindValue is by reference
                        // bindParam is by value
                        $statement->bindValue(':pName', $this->model->getName(), \PDO::PARAM_STR);
                        $statement->bindValue(':pPurchaseDate', $this->model->getPurchaseDate(), \PDO::PARAM_STR);
                        $statement->bindValue(':pPrice', $this->model->getPrice(), \PDO::PARAM_STR);
                        $result = $statement->execute();
                        // zet de nieuw toegekende Id in het Id veld van het model
                        $this->model->setId($this->pdo2->query('select @pId')->fetchColumn());
                    }
                } 
            }
            return $this->view("Home", "Inserting");
        }
        
        public function Delete()
        {
            if (isset($_GET['Id'])) {
                $deleteId = $_GET['Id'];
                $result = $this->pdo2->exec("call ArticleDelete($deleteId)");
            }
            
            $command = $this->pdo2->query("call ArticleSelectAll");
            $this->model->setList($command->fetchAll(\PDO::FETCH_ASSOC));
            return $this->view("Home", "Editing", $this->model);
        }
        
        public function Updating()
        {
            if ($this->pdo2){
            // derde parameter in het pad, is meestal een id
            $this->model->setId($this->route->getId());
            $statement = $this->pdo2->prepare("call ArticleSelectOne(:pId)"); 
            $statement->bindValue(':pId', $this->model->getId(), \PDO::PARAM_INT);
            $statement->execute();
            $articleOne = $statement->fetch(\PDO::FETCH_ASSOC);
            $this->model->setName($articleOne['Name']);
            $this->model->setPurchaseDate($articleOne['PurchaseDate']);
            $this->model->setPrice($articleOne['Price']);
            return $this->view('Home','Updating', $this->model);
            } else {
                return $this->view('Home', 'Error');
            }
        }
        
        public function Update()
        {
            
        }
    }
