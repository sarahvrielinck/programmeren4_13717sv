<?php
/*
* Class: Article
* Programmeren 4
*
* By Sarah on december 19th 2016
*/
    namespace Programmeren4\Article\Model;
    
    class Article extends \ModernWays\Mvc\Model
    {
        private $id;
        private $name;
        private $purchaseDate;
        private $price;
        
        public function getId()
        {
            return $this->id;
        }
        
        public function setId($id)
        {
            $this->id = $id;
        }
        
        public function getName ()
        {
            return $this->name;
        }
        
        public function setName ($name)
        {
            // alleen letters en cijfers en èéë en niet leeg
            if (strlen($name) == 0) {
                $this->modelState->startTimeInKey('Article Name');
                $this->modelState->setText('Naam kan niet leeg zijn!');
                $this->modelState->setCaption('Article Name insert');
                $this->modelState->setCode('007');
                $this->modelState->log();
            } else {
                if (preg_match("/^[a-zA-Z ]*$/", $name)) {
                     $this->name = $name;
                } else {
                    $this->modelState->startTimeInKey('Article Name');
                    $this->modelState->setText('Alleen letters en cijfers!');
                    $this->modelState->setCaption('Article Name insert');
                    $this->modelState->setCode('007');
                    $this->modelState->log();
                }
            }
        }
        
        public function getPurchaseDate()
        {
            return $this->purchaseDate;
        }
        
        public function setPurchaseDate($purchaseDate)
        {
            $this->purchaseDate = $purchaseDate;
        }
        
        public function getPrice ()
        {
            return $this->price;
        }
        
        public function setPrice($price)
        {
            $this->price = $price;
        }
        
        public function getVATIncluded()
        {
            return $this->price * 1.21;
        }
        
        public function getNameUpperCase()
        {
            return strtoupper($this->name);
        }
    }
    
?>