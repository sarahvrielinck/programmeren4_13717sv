<?php
    namespace Programmeren4\Les10\Controller;
    
    class Postcode extends \ModernWays\Mvc\Controller
    {
        public function index()
        {

            $textPostcalCodes = file_get_contents('data/Postcodes.csv');
            $postalCodes = explode("\n", $textPostcalCodes);
            $results = new \Programmeren4\Les10\Model\Postcode();
            $tmp = [];
            
            foreach ($postalCodes as $row) {
                $postCode = explode('|', $row);
                $result = new \Programmeren4\Les10\Model\Postcode();
                $result->postcode = $postCode[0];
                $result->stad = $postCode[1];
                $result->codePostal = $postCode[2];
                $result->ville = $postCode[3];
                $tmp[] = $result;
                
            }
            $results->setList($tmp);
            
            
            return $this->view('Postcode', 'Index', $results);
        }
    }