<?php
    namespace Programmeren4\Les10\Controller;
    
    class Biografie extends \ModernWays\Mvc\Controller
    {
        public function index()
        {
            $bio = new \Programmeren4\Les10\Model\Biografie();
            $bio->name = "Vrielinck";
            $bio->photoUri = "https://scontent-ams3-1.xx.fbcdn.net/v/t1.0-9/13343057_1103978796335949_6042482592659236574_n.jpg?oh=c38aedaa4cb3f61d0ce299cf6b69c391&oe=58B75E9E";
            $bio->firstName = "Sarah";
            $bio->birthday = "04/11/1989";
            $bio->placeOfBirth = "Brugge";
            $bio->father = "Rudi Vrielinck";
            $bio->mother = "Brenda Luyens";
            $bio->occupation = "Student, developer";
            $bio->officeAddress = "Hoboken";
            $bio->homeAddress = "Antwerpen";
            $bio->email = "vrielinck.s@gmail.com";
            $bio->phone = "133713371337";
            $bio->education = "Software and system development - Howest RSS";
            $bio->carreer = "Just started out ;-)";
            
            
            
            return $this->view("Biografie", "Index", $bio);
        }
    }