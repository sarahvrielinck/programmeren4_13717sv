<?php
    namespace Programmeren4\Les10\Model;
    
    class Biografie extends \ModernWays\Mvc\Model
    {
        public $name;
        public $photoUri;
        public $firstName;
        public $birthDay;
        public $placeOfBirth;
        public $father;
        public $mother;
        public $marriedTo = "No one";
        public $children = "None";
        public $occupation;
        public $officeAddress;
        public $homeAddress;
        public $email;
        public $phone;
        public $education = "None";
        public $carreer;
        public $publications = "None";
    }
?>