<!-- BIOGRAFIE VIEW PAGINA -->

<style>
    * {
        text-align: center;
    }
    
    img {
        border-radius: 2em;
    }
    
    body {
        height: 100%;
        width: 100%;
    }
    
    ul {
        list-style: none;
    }
    
    article {
        border-radius: 5em;
        /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#feffe8+0,d6dbbf+100;Wax+3D+%231 */
        background: rgb(254,255,232); /* Old browsers */
        background: -moz-linear-gradient(top,  rgba(254,255,232,1) 0%, rgba(214,219,191,1) 100%); /* FF3.6-15 */
        background: -webkit-linear-gradient(top,  rgba(254,255,232,1) 0%,rgba(214,219,191,1) 100%); /* Chrome10-25,Safari5.1-6 */
        background: linear-gradient(to bottom,  rgba(254,255,232,1) 0%,rgba(214,219,191,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#feffe8', endColorstr='#d6dbbf',GradientType=0 ); /* IE6-9 */
        margin: 0 auto;
        margin-left: 20em;
        margin-right: 20em;
        padding-top: 2em;
        padding-bottom: 1em;
    }
    header {
        background-color: white;
        color: gray;
        padding-top: 1em;
        padding-bottom: 1em;
    }
</style>

<article>
    <header>
        <h1>Biografie van <?php echo $model->firstName . " " . $model->name; ?></h3>
    </header>
        <h2>Personal biografie</h4>
        <img src="<?php echo $model->photoUri; ?>" alt="<?php echo $model->firstName; ?>" style="height: 20em; width: auto;"></img>
        <ul>
            <li>Birthday: <?php echo $model->birthday; ?></li>
            <li>Birthplace: <?php echo $model->placeOfBirth; ?></li>
            <li>Father: <?php echo $model->father; ?></li>
            <li>Mother: <?php echo $model->mother; ?></li>
            <li>Married to: <?php echo $model->marriedTo; ?></li>
            <li>Children: <?php echo $model->children; ?></li>
            <li>Occupation: <?php echo $model->occupation; ?></li>
            <li>Office address: <?php echo $model->officeAddress; ?></li>
            <li>Home address: <?php echo $model->homeAddress; ?></li>
            <li>Email: <?php echo $model->email; ?></li>
            <li>Phone: <?php echo $model->phone; ?></li>
        </ul>
        <h2>Education</h2>
        <?php echo $model->education; ?>
        <h2>Carreer and achievements</h2>
        <?php echo $model->carreer; ?>
        <h2>Publications</h2>
        <?php echo $model->publications; ?>
    </header>
</article>
