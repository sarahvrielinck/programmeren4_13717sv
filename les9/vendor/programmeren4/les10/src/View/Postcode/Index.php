<style>
        table th {
            padding: 1em;
            font-weight: bold;
            font-size: 120%;
            border-radius: 5em;
            background-color: green;
            color: white;
        }
        table td {
            padding: 1em;
        }
        table tr {
            background-color: white;
            color: black;
        }
        table tr:nth-child(even) {
            background-color: black;
            color: white;
        }
</style>
<table>
        <thead>
            <tr>
                <th>Postcode</th>
                <th>Stad</th>
                <th>Code Postal</th>
                <th>Ville</th>
            </tr>
        </thead>
<tbody>
<?php
    foreach($model->getList() as $postcode){
        echo "<tr>";
        echo "<td>" . $postcode->postcode . "</td>";
        echo "<td>" . $postcode->stad . "</td>";
        echo "<td>" . $postcode->codePostal . "</td>";
        echo "<td>" . $postcode->ville . "</td>";
        echo "</tr>";
    }
?>
</tbody>
</table>