<?php
    include("les8/vendor/modernways/dialog/src/Model/INotice.php");
    include("les8/vendor/modernways/dialog/src/Model/INoticeBoard.php");
    include("les8/vendor/modernways/dialog/src/Model/Notice.php");
    include("les8/vendor/modernways/dialog/src/Model/NoticeBoard.php");
    
    $noticeBoard = new \ModernWays\Dialog\Model\NoticeBoard();
    $noticeBoard->startTimeInKey("test dialog");
    $noticeBoard->setText("Mijn eerste foutmelding");
    $noticeBoard->setCaption("Dialog component testen");
    $noticeBoard->setCode("007");
    $noticeBoard->log();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dialog component</title>
</head>
<body>
    <pre><?php var_dump($noticeBoard); ?></pre>
    <?php
        $model = $noticeBoard;
        include("les8/vendor/modernways/dialog/src/View/NoticeBoard.php");
    ?>
</body>
</html>
