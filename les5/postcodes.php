<?php
$textPostcalCodes = file_get_contents('data/Postcodes.csv');
$postalCodes = explode("\n", $textPostcalCodes);
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Belgische postcodes</title>
    <style>
        table tr:nth-child(even) {
            background-color: red;
        }
    </style>
</head>
<body>
    
        <tbody>
    <?php
    foreach ($postalCodes as $row) {
        $postalCode = explode('|', $row);
        ?>
        <tr>
            <td><?php echo $postalCode[0];?></td>
            <td><?php echo $postalCode[1];?></td>
            <td><?php echo $postalCode[2];?></td>
            <td><?php echo $postalCode[3];?></td>
        </tr>
    <?php 
    } ?>
    </tbody>
    </table>    
</body>
</html>