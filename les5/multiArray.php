<?php
    $cars = array (
        array("naam" => "Volvo", "stock" => 22, "verkocht" => 18),
        array("naam" => "BMW", "stock" => 15, "verkocht" => 13),
        array("naam" => "Saab", "stock" => 5, "verkocht" => 5),
        array("naam" => "Land Rover", "stock" => 17, "verkocht" => 15)
    );
    
    $persons = array  (
        array("voornaam" => "Johanna", "familienaam" => "Den Doper", "leeftijd" => 117, "gender" => "vrouw"),
        array("voornaam" => "Sarah", "familienaam" => "Vrielinck", "leeftijd" => 27, "gender" => "vrouw")
    );
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Two-dimensional array</title>
</head>
<body>
    <table>
        <tr>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Leeftijd</th>
            <th>Gender</th>
        </tr>
        <?php
            foreach ($persons as $person) { 
        ?>
             <tr>
                 <td><?php echo $person["voornaam"]; ?></td>
                 <td><?php echo $person["familienaam"]; ?></td>
                 <td><?php echo $person["leeftijd"]; ?></td>
                 <td><?php echo $person["gender"]; ?></td>
             </tr>
        <?php
            }
        ?>
    </table>
</body>
</html>