<?php
namespace zazora;

// This class does not inherit from notice as it would break the 
// Single Responsibility Principle (SOLID)
 
class NoticeBoard
{
    // private variable as there is no need to access it outside the class
    private $notices = [];
    
    // this function adds an notice to the array
    public function addNotice ($notice) {
        $this->notices[] = $notice;
    }
    
    // this functions returns an array with all the notices
    public function getAllNotices () {
        return $this->notices;
    }
    
    // unset() won't change or reindex the array. array_values() could reset
    // indexes after using unset.
    //
    // This function calls the searchNotice function in orde to find the index
    // of the object that needs to be deleted. 
    public function removeNotice ($notice) {
        // http://php.net/manual/en/function.unset.php
        unset($this->notices[$this->searchNotice($notice->title)]);
        
        // http://php.net/manual/en/function.array-values.php
        array_values(); 
    }
    
    // Searches a notice based on text, owner, title, pushlishedOn and modified
    // Expected result: find obj in array based on the searchString given and
    // return its index in the array.
    public function searchNotice ($searchString) {
        $foundIndex = null;
        foreach ($this->notices as $obj) {
            if (($searchString == $obj->text) || 
               ($searchString == $obj->owner) ||
               ($searchString == $obj->title) ||
               ($searchString == $obj->publishedOn) ||
               ($searchString == $obj->modified)){
                $foundIndex = current($this->notices);
                break;
            }
        }
        return $foundIndex;
    }
}