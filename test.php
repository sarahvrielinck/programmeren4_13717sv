<?php

include (__DIR__ . '/les9/vendor/autoload.php'); // _DIR_ shows the root path
$appState = new \ModernWays\Dialog\Model\NoticeBoard();

//var_dump($appState);

// moeten we een route analyseren? *met klasse route
$request = new \ModernWays\Mvc\Request('/home/editing');
$route = new \ModernWays\Mvc\Route($appState, $request->uc());

// de namespace waarin de klassen staan van mijn app
// in de psr4 autoload moet ik dan het pad opgeven waar de klassen
// van die namespace staan
//
// de volgende methode maakt een instantie van de de controller home en voert 
// de methode index van die controller uit. Op voorwaarde dat er geen andere
// route wordt meegegeven. Default waarde.
$routeConfig = new \ModernWays\Mvc\RouteConfig('\Programmeren4\Article', $route, $appState);

// invokeAction voert de methdoe van de controller uit. REFLECTION
$view = $routeConfig->invokeActionMethod();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MVC webapp</title>
    <link rel="stylesheet" href="css/article.css" type="text/css"/>
</head>
<body>
    <?php $view(); ?>
</body>
</html>