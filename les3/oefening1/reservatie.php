<?php

$productNaamErr = $productPrijsErr = $genderErr = "";
$productNaam = $productPrijs = $gender = "";


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST['productNaam'])) {
        $productNaamErr = "Product naam is een verplicht veld";
    } else {
        $productNaam = $_POST["productNaam"];
    }
    
    $priceRegPattern = '/^[0-9]+(\.[0-9]{1,2})?$/';
    
    if (isset($_POST['pruductPrijs'])) {
        $price = $_POST['pruductPrijs'];
        if (empty($price)) {
            $productPrijsErr = "Gelieve een waarde te geven";
        } else if (preg_match($priceRegPattern, $price)) {
            $productPrijs = $price;
        } else {
            $productPrijsErr .= "Product prijs moet een decimale waarde zijn.";
        }
    }
}

if (isset($_POST['gender'])) {
    $gender = $_POST['gender'];
    switch($gender){
        case 0:
            $genderText = 'man';
            break;
        case 1:
            $genderText = 'vrouw';
            break;
        case 2:
            $genderText = 'anders';
            break;
        default:
            $genderText = 'Anarchist';
            break;
    }
}

if (isset($_POST['category'])){
    $category = $_POST['category'];
    switch ($category){
        case 0:
            $categoryText = "Huishoudspullen";
            break;
        case 1:
            $categoryText = "Electronica";
            break;
        case 2:
            $categoryText = "Snoep";
            break;
        case 3:
            $categoryText = "Boeken";
            break;
        default:
            $categoryText = "Onbekend";
            break;
    }
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Programmeren 4 - oefening 1</title>
</head>
<body>
    <h1>Zoekertje aanmaken</h1>
    <hr/>
    
    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
        
        <input type="text" name="productNaam" placeholder="product" required/>
        <span><?php echo $productNaamErr;?></span>
        <br/>
        <input type="text" name="pruductPrijs" placeholder="berag product in euros" required/>
        <span><?php echo $productPrijsErr;?></span>
        <br/>
        <div>
            <label for="man">Man</label>
            <input type="radio" id="gender" name="gender" value="0">
            <label for="vrouw">Vrouw</label>
            <input type="radio" name="gender" value="1" id="vrouw">
            <label for="other">Other</label>
            <input type="radio" id="other" name="gender" value="2" checked>
        </div>
        <br/>
        <div>
            <label for="category">Category</label>
            <select name="category" id="category">
                <option value="0">Huishoudspullen</option>
                <option value="1">Electronica</option>
                <option value="2">Snoep</option>
                <option value="3">Boeken</option>
            </select>
        </div>
        <br/>
        <button type="submit">aanmaken!</button>
    </form>
    <div>
        <?php
            echo "<p>Je bent vermoedelijk een {$genderText}</p>";
            echo "<p>De gekozen category is {$categoryText}</p>";
        ?>
    </div>
</body>
</html>