<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    
<pre>
    <?php
        //laat toe om een datagegeven te visualiseren op je scherm: soort debugging, in dit geval laat het je toe 
        // om te kijken hoe een superglobal eruit ziet.
        echo "superglobal : $_SERVER:\n";
        var_dump($_SERVER);
        
        echo "superglobal : $_REQUEST:\n";
        var_dump($_REQUEST);
        
        echo "superglobal : $_POST:\n";
        var_dump($_POST);
        
        echo "superglobal : $_GET:\n";
        var_dump($_GET);
    ?>
</pre>

<form method="post" action='<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>'>
    <input name="Voornaam" type="text" value="Sarah" placeholder="John"/>
    <button type="submit">Verzenden</button>
</form>

</body>
</html>